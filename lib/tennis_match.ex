defmodule TennisMatch do
  # Constructors
  def new_match_best_of(best_of) do
    %{
      state:      :playing,
      set_score:  ScoreCounter.giving_victory_at(minimum_point: div(best_of, 2) + 1, minimum_difference: 0),
      game_score: ScoreCounter.giving_victory_at(minimum_point: 6, minimum_difference: 2),
      points:     ScoreCounter.giving_victory_at(minimum_point: 4, minimum_difference: 2)
    }
  end

  def new_match do
    new_match_best_of(2)
  end

  # Testing
  def won_by?(a_tennis_match, a_player) do
    ScoreCounter.victory_for_player?(a_tennis_match.set_score, a_player)
  end

  # Accessing
  def set_score(a_tennis_match),    do: ScoreCounter.points(a_tennis_match.set_score)
  def games_score(a_tennis_match),  do: ScoreCounter.points(a_tennis_match.game_score)
  def points_score(a_tennis_match), do: ScoreCounter.points(a_tennis_match.points)

  # Actions
  def give_point_to_player(a_tennis_match, a_player) do
    assert_has_not_been_won(a_tennis_match)

    give_to_player(a_tennis_match, a_player, :points)
  end
  
  # PRIVATE
  def give_to_player(a_tennis_match, a_player, score_selector) do
    old_score_counter = a_tennis_match[score_selector]
    new_score_counter = ScoreCounter.incremented_for_player(old_score_counter, a_player)

    giving_point_handler_for(new_score_counter, a_player)
      .(a_tennis_match, a_player, score_selector, new_score_counter)
  end

  defp giving_point_handler_for(a_score, a_player) do
    if ScoreCounter.victory_for_player?(a_score, a_player) do
      &handle_giving_victory_point/4
    else
      &handle_giving_regular_point/4
    end
  end

  defp handle_giving_victory_point(a_tennis_match, a_player, :points, _new_points) do
    a_tennis_match |> reset_counter(:points) |> give_to_player(a_player, :game_score)
  end

  defp handle_giving_victory_point(a_tennis_match, a_player, :game_score, _new_points) do
    a_tennis_match |> reset_counter(:game_score) |> give_to_player(a_player, :set_score)
  end

  defp handle_giving_victory_point(a_tennis_match, _a_player, :set_score, new_points) do
    a_tennis_match |> update(:set_score, new_points) |> update(:state, :won)
  end

  defp handle_giving_regular_point(a_tennis_match, _a_player, score_selector, new_points) do
    a_tennis_match |> update(score_selector, new_points)
  end

  # Updates
  defp update(a_tennis_match, selector, new_value) do
    Map.replace!(a_tennis_match, selector, new_value)
  end

  def reset_counter(a_tennis_match, counter_selector) do
    old_counter = a_tennis_match[counter_selector]
    update(a_tennis_match, counter_selector, ScoreCounter.reset(old_counter))
  end

  # Assertions
  defp assert_has_not_been_won(a_tennis_match) do
    if a_tennis_match.state == :won do
      raise "Cannot give a point. Game has been won"
    end
  end
end
