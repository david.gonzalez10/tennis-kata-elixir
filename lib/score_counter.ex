defmodule ScoreCounter do
  # Constructors
  def new(victory_predicate) do
    new([0, 0], victory_predicate)
  end

  def giving_victory_at(minimum_point: minimum_point_for_victory, minimum_difference: minimum_difference) do
    new([0, 0], fn a_game_counter, a_player ->
      ScoreCounter.has?(a_game_counter, a_player, minimum_point: minimum_point_for_victory, minimum_difference: minimum_difference)
    end)
  end

  defp new(points, victory_predicate) do
    %{ points: points, victory_predicate: victory_predicate }
  end

  # Accessing
  def points(a_game_counter) do
    a_game_counter.points
  end

  def points_of_player(a_game_counter, a_player_number) do
    Enum.at(a_game_counter.points, a_player_number)
  end

  # Actions
  def reset(a_game_counter) do
    new(a_game_counter.victory_predicate)
  end

  def incremented_for_player(a_game_counter, a_player_number) do
    old_points = a_game_counter.points
    new_points = List.replace_at(old_points, a_player_number, Enum.at(old_points, a_player_number) + 1)

    new(new_points, a_game_counter.victory_predicate)
  end

  # Testing
  def has?(a_game_counter, a_player_number, minimum_point: minimum_point, minimum_difference: minimum_difference) do
    player_points = points_of_player(a_game_counter, a_player_number)
    opponent_points = points_of_player(a_game_counter, a_player_number - 1)

    player_points >= minimum_point && player_points - opponent_points >= minimum_difference
  end

  def victory_for_player?(a_game_counter, a_player_number) do
    a_game_counter.victory_predicate.(a_game_counter, a_player_number)
  end
end
