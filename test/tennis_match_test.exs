defmodule TennisMatchTest do
  use ExUnit.Case

  @moduletag :capture_log

  def first_player do
    0
  end

  def second_player do
    1
  end

  describe "Game." do
    test "starts with a game score of 0-0" do
      tennis_match = TennisMatch.new_match

      assert TennisMatch.points_score(tennis_match) == [0, 0]
    end

    test "the first player can win one point" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(first_player())

      assert TennisMatch.points_score(tennis_match) == [1, 0]
    end

    test "the first player can win many points" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())

      assert TennisMatch.points_score(tennis_match) == [2, 0]
    end

    test "the second player can win one point" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(second_player())

      assert TennisMatch.points_score(tennis_match) == [0, 1]
    end

    test "the second player can win many points" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(second_player())
                     |> TennisMatch.give_point_to_player(second_player())

      assert TennisMatch.points_score(tennis_match) == [0, 2]
    end

    test "when a player wins 4 points by a difference of 2 win a game" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(second_player())
                     |> TennisMatch.give_point_to_player(second_player())

                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())

      assert TennisMatch.games_score(tennis_match) == [1, 0]
    end

    test "when a player wins 4 points by a difference lesser than 2 does not win a game" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(second_player())
                     |> TennisMatch.give_point_to_player(second_player())
                     |> TennisMatch.give_point_to_player(second_player())

                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())

      assert TennisMatch.games_score(tennis_match) == [0, 0]
    end

    test "when a player wins more than 4 points by a difference of 2 win a game" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(second_player())
                     |> TennisMatch.give_point_to_player(second_player())
                     |> TennisMatch.give_point_to_player(second_player())

                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())

                     |> TennisMatch.give_point_to_player(first_player())

      assert TennisMatch.games_score(tennis_match) == [1, 0]
    end

    test "when a player wins a game the points counter of both players starts again" do
      tennis_match = TennisMatch.new_match
                     |> TennisMatch.give_point_to_player(second_player())

                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())
                     |> TennisMatch.give_point_to_player(first_player())

      assert TennisMatch.points_score(tennis_match) == [0, 0]
    end

    test "a player can win many games" do
      tennis_match = TennisMatch.new_match
                     |> win_games_for_first_player(2) 
      assert TennisMatch.games_score(tennis_match) == [2, 0]
    end

    test "both players can win a game" do
      tennis_match = TennisMatch.new_match
                     |> win_games_for_first_player(1)
                     |> win_games_for_second_player(1)
      
      assert TennisMatch.points_score(tennis_match) == [0, 0]
      assert TennisMatch.games_score(tennis_match) == [1, 1]
    end
  end

  describe "Set" do
    test "it's counter starts at 0-0" do
      tennis_match = TennisMatch.new_match

      assert TennisMatch.set_score(tennis_match) == [0, 0]
    end

    test "when a player wins 6 games by a difference of 2 win a set" do
      tennis_match =
        TennisMatch.new_match
        |> win_games_for_first_player(4)
        |> win_games_for_second_player(6)

      assert TennisMatch.set_score(tennis_match) == [0, 1]
      assert TennisMatch.games_score(tennis_match) == [0, 0]
      assert TennisMatch.points_score(tennis_match) == [0, 0]
    end

    test "when a player wins 6 games by a difference lesser than 2 does not win a set" do
      tennis_match =
        TennisMatch.new_match
        |> win_games_for_first_player(5)
        |> win_games_for_second_player(5)

        |> win_games_for_second_player(1)

      assert TennisMatch.set_score(tennis_match) == [0, 0]
      assert TennisMatch.games_score(tennis_match) == [5, 6]
      assert TennisMatch.points_score(tennis_match) == [0, 0]
    end
  end

  test "when a player wins more than 6 games by a difference of 2 wins a set" do
    tennis_match =
      TennisMatch.new_match
      |> win_games_for_first_player(5)
      |> win_games_for_second_player(5)

      |> win_games_for_second_player(2)

    assert TennisMatch.set_score(tennis_match) == [0, 1]
    assert TennisMatch.games_score(tennis_match) == [0, 0]
    assert TennisMatch.points_score(tennis_match) == [0, 0]
  end

  test "both players can win a set" do
    tennis_match = TennisMatch.new_match
                   |> win_games_for_first_player(6)
                   |> win_games_for_second_player(6)

    assert TennisMatch.set_score(tennis_match) == [1, 1]
    assert TennisMatch.games_score(tennis_match) == [0, 0]
    assert TennisMatch.points_score(tennis_match) == [0, 0]
  end

  test "when a player wins 2 sets wins the match" do
    tennis_match = TennisMatch.new_match
                   |> win_games_for_first_player(6)
                   |> win_games_for_first_player(6)

    assert TennisMatch.won_by?(tennis_match, 0) == true

    assert TennisMatch.set_score(tennis_match) == [2, 0]
    assert TennisMatch.games_score(tennis_match) == [0, 0]
    assert TennisMatch.points_score(tennis_match) == [0, 0]
  end

  test "when a player wins less than 2 sets does not wins the match" do
    tennis_match = TennisMatch.new_match
                   |> win_games_for_first_player(6)

    assert TennisMatch.won_by?(tennis_match, 0) == false

    assert TennisMatch.set_score(tennis_match) == [1, 0]
    assert TennisMatch.games_score(tennis_match) == [0, 0]
    assert TennisMatch.points_score(tennis_match) == [0, 0]
  end

  test "cannot mark a point on a won match" do
    tennis_match = TennisMatch.new_match |> win_games_for_first_player(6) |> win_games_for_first_player(6)

    assert_raise RuntimeError, "Cannot give a point. Game has been won", fn ->
      TennisMatch.give_point_to_player(tennis_match, first_player())
    end

    assert TennisMatch.set_score(tennis_match) == [2, 0]
    assert TennisMatch.games_score(tennis_match) == [0, 0]
    assert TennisMatch.points_score(tennis_match) == [0, 0]
  end

  test "a match can be played for a best of any positive natural number" do
    tennis_match = TennisMatch.new_match_best_of(5)
                   |> win_games_for_first_player(6)
                   |> win_games_for_first_player(6)

                   |> win_games_for_second_player(6)
                   |> win_games_for_second_player(6)
                   |> win_games_for_second_player(6)

    assert TennisMatch.won_by?(tennis_match, second_player()) == true
    assert TennisMatch.set_score(tennis_match) == [2, 3]
    assert TennisMatch.games_score(tennis_match) == [0, 0]
    assert TennisMatch.points_score(tennis_match) == [0, 0]
  end

  def win_games_for_first_player(a_tennis_match, number_of_repetitions) do
    Enum.reduce(
      1..number_of_repetitions,
      a_tennis_match,
      fn (_, match) ->
        match
        |>TennisMatch.give_point_to_player(first_player())
        |>TennisMatch.give_point_to_player(first_player())
        |>TennisMatch.give_point_to_player(first_player())
        |>TennisMatch.give_point_to_player(first_player()) end)
  end

  def win_games_for_second_player(a_tennis_match, number_of_repetitions) do
    Enum.reduce(
      1..number_of_repetitions,
      a_tennis_match,
      fn (_, match) ->
        match
          |>TennisMatch.give_point_to_player(second_player())
          |>TennisMatch.give_point_to_player(second_player())
          |>TennisMatch.give_point_to_player(second_player())
          |>TennisMatch.give_point_to_player(second_player()) end)
  end
end
